import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

import * as card from './card'

import { useAccessor } from 'typed-vuex'

Vue.use(Vuex)

const storePattern = {
  modules: {
    card,
  },
  plugins: [createPersistedState()],
}

const store = new Vuex.Store(storePattern)

export const accessor = useAccessor(store, storePattern)

Vue.prototype.$accessor = accessor

export default store
