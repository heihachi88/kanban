// import Vue from 'vue'
import { getterTree, mutationTree, actionTree } from 'typed-vuex'
import { Card } from '@/interfaces/card'

export const namespaced = true

interface State {
  (): {
    columns: Cards[]
  }
}

interface Cards {
  cards: Card[]
}

export const state: State = () => ({
  columns: [
    {
      cards: [
        {
          id: 1,
          title: 'Card number one',
          description:
            'Lorem ipsum dolar sit amet Lorem ipsum dolar sit amet Lorem ipsum dolar sit amet Lorem ipsum dolar sit amet',
        },
      ],
    },
    {
      cards: [
        {
          id: 2,
          title: 'Card number two',
          description:
            'Some logn description here Lorem ipsum dolar sit amet Lorem ipsum dolar sit amet',
        },
      ],
    },
    {
      cards: [],
    },
    {
      cards: [],
    },
  ],
})

export const getters = getterTree(state, {
  column: (state) => (column: number) => {
    return state.columns[column].cards
  },
})

export const mutations = mutationTree(state, {
  ADD_CARD(state, { data, column }: { data: Card; column: number }) {
    state.columns[column].cards.push(data)
  },
  UPDATE_CARDS(state, { data, column }: { data: Card[]; column: number }) {
    state.columns[column].cards = data
  },
})

export const actions = actionTree(
  { state, getters, mutations },
  {
    async addCard(
      { commit },
      { data, column }: { data: Card; column: number }
    ) {
      commit('ADD_CARD', { data, column })
    },
    updateCards(
      { commit },
      { data, column }: { data: Card[]; column: number }
    ) {
      commit('UPDATE_CARDS', { data, column })
    },
  }
)
