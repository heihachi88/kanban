export interface Card {
  id: number
  title: string | null
  description: string | null
}
